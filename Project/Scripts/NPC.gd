extends "Person.gd"

class_name NPC


const enums = preload("res://Scripts/Global/Enums.gd")

signal npc_touched(npc, body)
signal npc_request_move(npc)
signal npc_gave_money(amount)

export(int) var hp : int = 5
export(enums.ItemType) var gives_item : int = enums.ItemType.NONE
export(String) var give_dialog = "Here you go"
export(enums.ItemType) var buys_item : int = enums.ItemType.NONE
export(int) var buy_charisma : int = 1
export(bool) var repeat_buy : bool = false
export(String) var buy_dialog = "Thanks!"
export(String) var reject_dialog = "I don't trust you enough to buy that from you."
export(String) var dont_want_dialog = "I don't want that."
export(String) var greetings_dialog = "Hello!"
export(enums.Stat) var favorite_stat : int = enums.Stat.NONE
export(bool) var hostile = false
export(enums.ItemType) var item_dropped : int = enums.ItemType.NONE
export(String) var knockout_dialog = "Oof my head..."

onready var has_bought = false


func _ready() -> void:
	var game = get_tree().get_root().get_node("Game")
	if game != null:
		connect("npc_touched", game, "_npc_touched")
		connect("npc_request_move", game, "_npc_request_move")
		connect("npc_gave_money", game, "_npc_gave_money")


func set_dialog(new_dialog : String) -> void:
	$FloatingDialog.set_text(new_dialog)


func interact(player_state : PlayerState) -> void:
	if hostile:
		$FloatingDialog.set_text(greetings_dialog)
	elif player_state.has_item and !has_bought:
		if player_state.item.item_type == buys_item:
			if player_state.get_stat(enums.Stat.CHARISMA) >= buy_charisma:
				$FloatingDialog.set_text(buy_dialog)
				player_state.item.destroy()
				player_state.clear_item()
				var price = _calculate_buy_price(player_state)
				emit_signal("npc_gave_money", price)
				if !repeat_buy:
					has_bought = true
			else:
				$FloatingDialog.set_text(reject_dialog)
		else:
			$FloatingDialog.set_text(dont_want_dialog)
	else:
		$FloatingDialog.set_text(greetings_dialog)
	$FloatingDialog.visible = true
	$DialogTimer.start(7)


func _calculate_buy_price(player_state : PlayerState) -> int:
	if favorite_stat == enums.Stat.NONE:
		return 1
	var relevant_stat = player_state.get_stat(favorite_stat)
	var bonus = floor((relevant_stat-10) / 10)
	return 1 + bonus


func knock_out() -> void:
	.knock_out()
	$FloatingDialog.set_text(knockout_dialog)
	$FloatingDialog.visible = true
	$DialogTimer.start(7)


func _on_AreaMask_body_entered(body : PhysicsBody2D) -> void:
	if !knocked_out:
		emit_signal("npc_touched", self, body)


func _on_DialogTimer_timeout() -> void:
	$FloatingDialog.visible = false
	$DialogTimer.stop()


func _on_AreaMask_mouse_entered() -> void:
	$Highlight.visible = true


func _on_AreaMask_mouse_exited() -> void:
	$Highlight.visible = false


func _on_MoveTimer_timeout() -> void:
	if !knocked_out and !moving:
		emit_signal("npc_request_move", self)
