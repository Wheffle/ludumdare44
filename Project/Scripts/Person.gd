extends KinematicBody2D

class_name Person


signal person_clicked(person)

const ANIM_IDLE = "Idle"
const ANIM_ACTION = "Action"
const ANIM_WALKING = "Walking"

export(float) var speed : float = 200
export(int) var strength = 10
export(int) var intelligence = 10
export(int) var charisma = 10

var moving : bool = false
var path : Array = []
var path_index : int = 0

var doing_work : bool = false setget _set_doing_work
var knocked_out : bool = false

func _physics_process(delta : float) -> void:
	if !knocked_out and moving:
		var next_position : Vector2 = path[path_index]
		var direction : Vector2 = (next_position - global_position).normalized()
		_update_sprite_direction(direction)
		var displacement = direction * speed
		move_and_slide(displacement)
		if global_position.distance_squared_to(next_position) <= (displacement*delta).length_squared():
			path_index += 1
			if path_index >= path.size():
				moving = false
				_set_animation(ANIM_IDLE)


func set_destination(destination : Vector2) -> void:
	path = [destination]
	path_index = 0
	moving = true
	_set_animation(ANIM_WALKING)


func set_path(point_array : Array) -> void:
	path = point_array
	path_index = 0
	if path.size() > 0:
		moving = true
		_set_animation(ANIM_WALKING)


func get_hold_position() -> Vector2:
	return $HoldPosition.global_position


func knock_out() -> void:
	knocked_out = true
	_set_animation(ANIM_IDLE)
	$AnimatedSprite.rotate(-PI/2)


func _set_animation(animation : String) -> void:
	if doing_work and animation == ANIM_IDLE:
		$AnimatedSprite.animation = ANIM_ACTION
	else:
		$AnimatedSprite.animation = animation


func _set_doing_work(value : bool) -> void:
	doing_work = value
	if doing_work and $AnimatedSprite.animation == ANIM_IDLE:
		_set_animation(ANIM_ACTION)
	elif $AnimatedSprite.animation == ANIM_ACTION:
		_set_animation(ANIM_IDLE)


func _update_sprite_direction(direction : Vector2) -> void:
	if direction.x < 0:
		$AnimatedSprite.scale.x = -abs($AnimatedSprite.scale.x)
		$HoldPosition.position.x = -abs($HoldPosition.position.x)
	else:
		$AnimatedSprite.scale.x = abs($AnimatedSprite.scale.x)
		$HoldPosition.position.x = abs($HoldPosition.position.x)


func _on_AreaMask_input_event(viewport : Viewport, event : InputEvent, shape_idx : int) -> void:
	if event is InputEventMouseButton:
		var mouse_event : InputEventMouseButton = event
		if mouse_event.pressed and mouse_event.button_index == BUTTON_LEFT:
			emit_signal("person_clicked", self)
