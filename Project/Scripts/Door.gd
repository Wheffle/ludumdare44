extends Area2D


onready var bodies_in_door = 0


func _ready() -> void:
	_update_sprite()


func _update_sprite() -> void:
	if bodies_in_door > 0:
		$Sprite.frame = 1
	else:
		$Sprite.frame = 0


func _on_Door_body_entered(body : PhysicsBody2D) -> void:
	if body is Person:
		bodies_in_door += 1
		_update_sprite()


func _on_Door_body_exited(body : PhysicsBody2D) -> void:
	if body is Person:
		bodies_in_door -= 1
		_update_sprite()

