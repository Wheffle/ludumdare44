extends Node

class_name PathControl


const enums = preload("res://Scripts/Global/Enums.gd")

var astar : AStar
var width : int
var height : int
var depth : int
var cell_size : int
var passmap := {}


func _init(grid_width : int, grid_height : int, grid_depth : int, grid_cell_size : int) -> void:
	width = grid_width
	height = grid_height
	depth = grid_depth
	cell_size = grid_cell_size
	astar = AStar.new()
	for i in width:
		for j in height:
			for k in depth:
				var node_id = astar.get_available_point_id()
				var node_position = Vector3(i, j, k)
				astar.add_point(node_id, node_position)
				set_node_passable(node_position, true)


func find_path_2d(position1 : Vector2, position2 : Vector2) -> Array:
	var v1 = Vector3(position1.x, position1.y, 0) / cell_size
	var v2 = Vector3(position2.x, position2.y, 0) / cell_size
	var node1 = astar.get_closest_point(v1)
	var node2 = astar.get_closest_point(v2)
	var pool_array : PoolVector3Array = astar.get_point_path(node1, node2)
	var array = []
	for point in pool_array:
		array.append(Vector2(point.x*cell_size, point.y*cell_size))
	return array


func find_path(position1 : Vector3, position2 : Vector3) -> Array:
	var node1 = astar.get_closest_point(position1 / cell_size)
	var node2 = astar.get_closest_point(position2 / cell_size)
	var pool_array : PoolVector3Array = astar.get_point_path(node1, node2)
	var array = []
	for point in pool_array:
		array.append(point * cell_size)
	return array


func set_node_passable(position : Vector3, is_passable : bool) -> void:
	if _position_in_bounds(position):
		var node_id = astar.get_closest_point(position)
		passmap[node_id] = is_passable
		_refresh_planar_node_connections(node_id)


func is_node_passable(position : Vector3) -> bool:
	if _position_in_bounds(position):
		var node_id = astar.get_closest_point(position)
		return passmap[node_id]
	return false


func _refresh_planar_node_connections(node_id : int) -> void:
	var node_passable = passmap[node_id]
	var neighbors = _get_planar_neighbors(node_id)
	for neighbor_id in neighbors:
		var should_be_connected = (node_passable and passmap[neighbor_id])
		var are_connected = astar.are_points_connected(node_id, neighbor_id)
		if should_be_connected and !are_connected:
			astar.connect_points(node_id, neighbor_id, true)
		elif !should_be_connected and are_connected:
			astar.disconnect_points(node_id, neighbor_id)


func _position_in_bounds(position : Vector3) -> bool:
	if position.x < 0 or position.y < 0 or position.z < 0:
		return false
	if position.x >= width or position.y >= height or position.z >= depth:
		return false
	return true


func _get_planar_neighbors(node_id : int) -> Array:
	var node_position = astar.get_point_position(node_id)
	var neighbors = []
	for dir in enums.directions_planar.values():
		var neighbor_position = node_position + dir
		if _position_in_bounds(neighbor_position):
			var neighbor_id = astar.get_closest_point(neighbor_position)
			neighbors.append(neighbor_id)
	return neighbors



