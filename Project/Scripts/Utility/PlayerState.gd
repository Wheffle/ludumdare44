extends Node

class_name PlayerState


const enums = preload("res://Scripts/Global/Enums.gd")

var money : int = 0
var health : int = 10
var max_health : int = 10
var stats := {}
var item : Item setget _set_item
var has_item : bool = false


func _init() -> void:
	for stat in enums.Stat.values():
		stats[stat] = 1


func add_money(value : int) -> void:
	money += value


func get_stat(stat : int) -> int:
	return stats[stat]


func set_stat(stat : int, value : int) -> void:
	stats[stat] = value


func add_stat(stat : int, value : int) -> void:
	var old_value = stats[stat]
	stats[stat] = old_value + value


func _set_item(new_item : Item) -> void:
	item = new_item
	if item == null:
		has_item = false
	else:
		has_item = true


func clear_item() -> void:
	item = null
	has_item = false