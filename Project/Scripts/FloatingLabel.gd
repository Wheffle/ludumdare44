extends Node2D


export(int) var speed = 20


func _process(delta) -> void:
	translate(Vector2(0, -speed * delta))


func set_text(new_text : String) -> void:
	$Label.text = new_text


func set_color(new_color : Color) -> void:
	$Label.set("custom_colors/font_color", new_color)


func _on_Timer_timeout():
	get_parent().remove_child(self)
	queue_free()
