extends Area2D

class_name Station


signal station_entered(station, body)
signal station_exited(station, body)
signal stat_improved(stat)

const enums = preload("res://Scripts/Global/Enums.gd")

const ANIM_ACTION = "Action"

export(enums.Stat) var stat_improved = enums.Stat.STRENGTH
export(float) var seconds_required = 5

onready var highlight = $AnimatedSprite/Highlight

var activated : bool = false


func _ready() -> void:
	var game = get_tree().get_root().get_node("Game")
	connect("station_entered", game, "_station_entered")
	connect("station_exited", game, "_station_exited")
	connect("stat_improved", game, "_stat_improved")


func activate() -> void:
	activated = true
	$Timer.start(seconds_required)
	$AnimatedSprite.play(ANIM_ACTION)


func deactivate() -> void:
	activated = false
	$Timer.stop()


func _on_AnimatedSprite_animation_finished() -> void:
	if !activated:
		$AnimatedSprite.stop()
		$AnimatedSprite.frame = 0


func _on_Timer_timeout() -> void:
	if activated:
		emit_signal("stat_improved", stat_improved)


func _on_Station_body_entered(body : PhysicsBody2D) -> void:
	emit_signal("station_entered", self, body)


func _on_Station_body_exited(body : PhysicsBody2D) -> void:
	emit_signal("station_exited", self, body)


func _on_AreaMask_mouse_entered():
	highlight.visible = true


func _on_AreaMask_mouse_exited():
	highlight.visible = false


