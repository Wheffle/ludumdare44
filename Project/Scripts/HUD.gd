extends Control


const enums = preload("res://Scripts/Global/Enums.gd")

signal item_eat
signal item_drop

onready var health_label = $StatsContainer/VBoxContainer/HealthContainer/Label
onready var strength_label = $StatsContainer/VBoxContainer/StrengthContainer/Label
onready var intelligence_label = $StatsContainer/VBoxContainer/IntelligenceContainer/Label
onready var charisma_label = $StatsContainer/VBoxContainer/CharismaContainer/Label
onready var daysleft_label = $UpperContainer/ReaperContainer/VBoxContainer/HBoxContainer/Label
onready var money_label = $UpperContainer/ReaperContainer/VBoxContainer/MoneyLabel
onready var item_texture = $ItemDropContainer/VBoxContainer/TextureRect
onready var item_eat_button = $ItemDropContainer/VBoxContainer/EatButton
onready var item_drop_button = $ItemDropContainer/VBoxContainer/DropButton
onready var time_bar = $DayContainer/TimeContainer/VBoxContainer/ProgressBar

onready var time_bar_fg = time_bar.get("custom_styles/fg")
onready var time_bar_color_normal : Color = time_bar_fg.bg_color
onready var time_bar_color_flashing : Color = Color(1, 1, 1, 1)


func update_health(value : int) -> void:
	health_label.text = str(value)


func update_stat(stat : int, value : int) -> void:
	match stat:
		enums.Stat.STRENGTH:
			strength_label.text = str(value)
		enums.Stat.INTELLIGENCE:
			intelligence_label.text = str(value)
		enums.Stat.CHARISMA:
			charisma_label.text = str(value)


func update_days_left(value : int) -> void:
	daysleft_label.text = str(value)


func update_money(value : int) -> void:
	money_label.text = "$" + str(value)


func set_item(item : Item) -> void:
	item_texture.texture = item.texture
	if item.edible:
		item_eat_button.disabled = false
	item_drop_button.disabled = false
	$ItemDropContainer.visible = true


func clear_item() -> void:
	item_eat_button.disabled = true
	item_drop_button.disabled = true
	$ItemDropContainer.visible = false


func _on_EatButton_pressed():
	emit_signal("item_eat")


func _on_DropButton_pressed():
	emit_signal("item_drop")


func set_time(new_value : float) -> void:
	time_bar.value = new_value


func flash_time_bar() -> void:
	time_bar_fg.bg_color = time_bar_color_flashing
	$ColorFlashTimer.start(0.25)


func _on_ColorFlashTimer_timeout():
	$ColorFlashTimer.stop()
	time_bar_fg.bg_color = time_bar_color_normal
