extends Area2D

class_name Item


signal item_touched(item, body)

const enums = preload("res://Scripts/Global/Enums.gd")

export(enums.ItemType) var item_type : int = enums.ItemType.PAPER
export(bool) var edible = false
export(float) var eat_bonus = 0

onready var texture = $Sprite.texture

var held : bool = false
var holding_person : Person
var disable_pickup : bool = false
var wait_for_body : PhysicsBody2D


func _ready() -> void:
	var game = get_tree().get_root().get_node("Game")
	if game != null:
		connect("item_touched", game, "_item_touched")


func _process(delta : float) -> void:
	if held:
		global_position = holding_person.get_hold_position()


func set_holding_person(person : Person) -> void:
	$Highlight.visible = false
	held = true
	holding_person = person


func clear_holding_person() -> void:
	held = false


func wait_for_body_to_leave(body : PhysicsBody2D) -> void:
	disable_pickup = true
	wait_for_body = body


func _on_Item_body_entered(body : PhysicsBody2D) -> void:
	if !held and !disable_pickup:
		emit_signal("item_touched", self, body)


func _on_Item_body_exited(body : PhysicsBody2D) -> void:
	if !held and disable_pickup and body == wait_for_body:
		disable_pickup = false


func _on_Item_mouse_entered() -> void:
	if !held:
		$Highlight.visible = true


func _on_Item_mouse_exited() -> void:
	$Highlight.visible = false


func destroy() -> void:
	get_parent().remove_child(self)
	queue_free()


