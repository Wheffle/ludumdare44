extends Node2D


const enums = preload("res://Scripts/Global/Enums.gd")
const FloatingLabel = preload("res://Scenes/FloatingLabel.tscn")

const cell_size = 16

onready var player = $YSort/Player
onready var hud = $YSort/Player/Camera2D/CanvasLayer/HUD
onready var structure_map = $YSort/StructureMap
onready var nav = $Navigation2D

onready var grid_width : int = 75
onready var grid_height : int = 75

onready var player_state : PlayerState = PlayerState.new()

var current_time : float = 1
var mouse_down : bool = false


func _ready() -> void:
	hud.update_health(player_state.health)
	for stat in enums.Stat.values():
		hud.update_stat(stat, player_state.get_stat(stat))


func _process(delta : float) -> void:
	if mouse_down:
		var mouse_position : Vector2 = get_global_mouse_position()
		player.set_destination(mouse_position)


func _unhandled_input(event : InputEvent) -> void:
	if event is InputEventMouseButton:
		var mouse_event : InputEventMouseButton = event
		if mouse_event.button_index == BUTTON_LEFT:
			if mouse_event.is_pressed():
				mouse_down = true
			else:
				mouse_down = false


func _stat_improved(stat : int) -> void:
	player_state.add_stat(stat, 1)
	hud.update_stat(stat, player_state.get_stat(stat))
	_spawn_plus_one(player.global_position - Vector2(0, 70))


func _spawn_plus_one(label_position : Vector2) -> void:
	var floating_label = FloatingLabel.instance()
	floating_label.global_position = label_position
	add_child(floating_label)


func _station_entered(station : Station, body : PhysicsBody2D) -> void:
	if body == player:
		player.doing_work = true
		station.activate()


func _station_exited(station : Station, body : PhysicsBody2D) -> void:
	if body == player:
		player.doing_work = false
		station.deactivate()


func _npc_touched(npc : NPC, body : PhysicsBody2D) -> void:
	if body == player:
		npc.interact(player_state)
		if npc.hostile:
			player.set_target(npc)


func _npc_request_move(npc : NPC) -> void:
	var p2d : Position2D = _get_random_place()
	var path : Array = nav.get_simple_path(npc.global_position, p2d.global_position)
	npc.set_path(path)


func _npc_gave_money(amount : int) -> void:
	_increase_money(amount)


func _increase_money(amount : int) -> void:
	player_state.add_money(amount)
	hud.update_money(player_state.money)
	var floating_label = FloatingLabel.instance()
	floating_label.global_position = player.global_position - Vector2(0, 70)
	floating_label.set_text("+$" + str(amount))
	floating_label.set_color(Color(0, 1, 0, 1))
	add_child(floating_label)


func _get_random_place() -> Position2D:
	var index = randi() % $Places.get_child_count()
	var p2d : Position2D = $Places.get_child(index)
	return p2d


func _item_touched(item : Item, body : PhysicsBody2D) -> void:
	if body == player and !player_state.has_item:
		item.set_holding_person(player)
		player_state.item = item
		hud.set_item(item)


func _on_HUD_item_eat():
	if player_state.has_item:
		current_time = clamp(current_time + player_state.item.eat_bonus, 0, 1)
		player_state.item.destroy()
		player_state.clear_item()
	hud.clear_item()


func _on_HUD_item_drop():
	if player_state.has_item:
		player_state.item.wait_for_body_to_leave(player)
		player_state.item.global_position = player.global_position
		player_state.item.clear_holding_person()
		hud.clear_item()
		player_state.clear_item()


func _on_Timer_timeout():
	if player.doing_work:
		current_time = clamp(current_time - 0.01, 0, 1)
		hud.flash_time_bar()
	else:
		current_time = clamp(current_time - 0.001, 0, 1)
	hud.set_time(current_time)


