extends "Person.gd"


const PunchEffect = preload("res://Scenes/SpecialEffects/PunchEffect.tscn")

var target : NPC
var has_target : bool = false
var hostile_distance_squared = 96 * 96


func set_target(new_target : NPC) -> void:
	target = new_target
	has_target = true
	$HostileTimer.start()


func _on_HostileTimer_timeout():
	if has_target and target.global_position.distance_squared_to(global_position) <= hostile_distance_squared:
		var punch_effect = PunchEffect.instance()
		punch_effect.global_position = target.global_position - Vector2(0, 32)
		get_tree().get_root().get_node("Game").add_child(punch_effect)
	else:
		has_target = false
		$HostileTimer.stop()
