extends Node


enum Stat {
	NONE,
	STRENGTH,
	INTELLIGENCE,
	CHARISMA
	}

enum ItemType {
	NONE,
	PAPER,
	TWINKIE
	}

enum {NORTH, SOUTH, EAST, WEST, UP, DOWN}

const directions := {
	NORTH : Vector3(0, -1, 0),
	SOUTH : Vector3(0, 1, 0),
	EAST : Vector3(1, 0, 0),
	WEST : Vector3(-1, 0, 0),
	UP : Vector3(0, 0, -1),
	DOWN : Vector3(0, 0, 1)
	}

const directions_planar := {
	NORTH : Vector3(0, -1, 0),
	SOUTH : Vector3(0, 1, 0),
	EAST : Vector3(1, 0, 0),
	WEST : Vector3(-1, 0, 0)
	}